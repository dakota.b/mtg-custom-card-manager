<?php
class Card {
    public $name;
    public $rarity;
    public $manaCost;
    public $type;
    public $text;
    public $flavourText;
    public $power;
    public $toughness;
    public $layout;
    public $colourIdentity;

    public static function from_json($json) {
        $json = json_decode($json);
        

        return Card::from_obj($json);
    }

    public static function from_obj($obj) {
        $card = new Card();

        $card->name = $obj->name;
        $card->rarity = $obj->rarity;
        $card->manaCost = $obj->manaCost;
        $card->type = $obj->type;
        $card->text = $obj->text;
        $card->flavourText = $obj->flavourText;
        $card->power = $obj->power;
        $card->toughness = $obj->toughness;
        $card->layout = $obj->layout;
        $card->colourIdentity = $obj->colourIdentity;
        $card->quantity = 1;

        return $card;
    }
}

class CardCollection {
    public $db = [];
    public $modified = false;

    public function __construct() {
        if (!file_exists(__DIR__ . '/data/'))
            mkdir(__DIR__ . '/data/');
        if (file_exists(__DIR__ . '/data/cards.db')) {
            $this->db = json_decode(file_get_contents(__DIR__ . '/data/cards.db'));

        }

        $temp = array();

        foreach($this->db as $k => $va) {
            $temp[$k] = [];
            foreach($va as $vc) {
                array_push($temp[$k], Card::from_obj($vc));
            }
        }

        $this->db = $temp;
    }

    function pull_card_data($cardname) {
        system('python get_card_info.py "' . addslashes($cardname) . '"');
        return Card::from_json(json_decode(file_get_contents('card.json')));
    }

    function commit_db() {
        file_put_contents(__DIR__ . '/data/cards.db', json_encode($this->db));
    }

    function add_card_to_db(Card $card) {
        if ($card === null)
            return;

        $ci = implode($card->colourIdentity);
        $matched = false;
        if (array_key_exists($ci, $this->db)) {
            foreach($this->db[$ci] as $i => $o) {
                if ($o->name == $card->name) {
                    $o->quantity++;
                    $matched = true;
                }
            }
        } else {
            $this->db[$ci] = array();
        }

        if ($matched == false) {
            array_push($this->db[$ci], $card);
        }

        $modified = true;
    }

    function purge_card_from_db(Card $card) {
        if (!isset($card))
            return;
        $ci = implode($card->colourIdentity);
        if (array_key_exists($ci, $this->db)) {
            foreach($this->db[$ci] as $i => $o) {
                if ($o->name == $card->name) {
                    array_splice($this->db[$ci], $i, 1);
                }
            }
        }

        $modified = true;
    }

    function set_card_quantity(Card $card) {
        $ci = implode($card->colourIdentity);
        if (array_key_exists($ci, $this->db)
        && in_array($this->db[$ci])) {
            foreach($this->db[$ci] as $i => $o) {
                if ($o->name == $card->name) {
                    $o->quantity = $quantity;
                }
            }
        } else {
            $this->add_card_to_db($card);
        }
    }

    function update_card($card_name, Card $changes) {
        $c = search_cards($card_name);
        purge_card_from_db($c);

        $c->name = $changes->name ? $changes->name : $c->name;
        $c->rarity = $changes->rarity ? $changes->rarity : $c->rarity;
        $c->manaCost = $changes->manaCost ? $changes->manaCost : $c->manaCost;
        $c->type = $changes->type ? $changes->type : $c->type;
        $c->text = $changes->text ? $changes->text : $c->text;
        $c->flavourText = $changes->flavourText ? $changes->flavourText : $c->flavourText;
        $c->power = $changes->power ? $changes->power : $c->power;
        $c->toughness = $changes->toughness ? $changes->toughness : $c->toughness;
        $c->layout = $changes->layout ? $changes->layout : $c->layout;
        $c->colourIdentity = $changes->colourIdentity ? $changes->colourIdentity : $c->colourIdentity;

        add_card_to_db($c);
    }

    function search_cards($card_name) {
        $card_name = strtolower($card_name);
        foreach($this->db as $coll) {
            foreach($coll as $c) {
                if (strtolower($c->name) == $card_name) {
                    return $c;
                }
            }
        }
    }
}

class CardManager {
    public $db;
    public $in;
    public $interruptus = false;

    public function __construct() {
        $this->db = new CardCollection();

        $this->in = fopen ("php://stdin","r");
        stream_set_blocking($this->in, false);


        $this->main();
    }

    public function getline() {
        if (!$this->interruptus) {
            $line = trim(fgets($this->in));
            $this->interruptus = $line == '!interruptus';
            return $line;
        }
    }

    public function main() {
        while(true) {
            echo 'Please select an option' . PHP_EOL;
            echo '1. Add a custom card to database' . PHP_EOL;
            echo '2. Add a card from scryfall' . PHP_EOL;
            echo '3. Remove a card from database' . PHP_EOL;
            echo '4. Edit a card in database' . PHP_EOL;
            echo '5. ' . ($this->db->modified ? '[*]' : '') . 'Save changes' . PHP_EOL;
            echo '6. View cards' . PHP_EOL;
            echo '7. Exit' . PHP_EOL;

            switch(intval($this->getline())) {
                case 1:
                    $this->db->add_card_to_db($this->request_card());
                    break;
                case 2:
                    $this->db->add_card_to_db($this->request_scryfall_card());
                    break;
                case 3:
                    $this->db->purge_card_from_db($this->request_card_by_name());
                    break;
                case 4:
                    $this->db->update_card($this->request_card());
                    break;
                case 5:
                    $this->db->commit_db();
                    break;
                case 6:
                    echo ">>> Starting server... Press Ctrl+C to quit." . PHP_EOL;
                    system('start CardManager-LaunchPad.html');
                    system('php\php -S localhost:6969');
                    break;
                case 7:
                    die('Thank you for using MTG card database management software :)' . PHP_EOL);
                    break;
                default:

            }
        }
    }

    public function request_card_by_name($card_name = null) {
        if ($card_name === null) {
            echo 'Please enter the name of the card' . PHP_EOL;
            $card_name = $this->getline();
        }
        return $this->db->search_cards($card_name);
    }

    public function request_scryfall_card($cname = null) {
        echo 'Please enter the name of the official card' . ($cname ? " [$cname]" : '') . PHP_EOL;
        $name = $this->getline();
        if (empty($name))
            $name = $cname;
        return $this->db->pull_card_data($name);
    }

    public function request_card() {
        $card = new Card();
        $scryfall_condition = 0;
        $scryfall_card = null;

        echo 'What is the name of the card?' . PHP_EOL;
        if (!($card->name = $this->getline())) return null;

        // ask if the user wants to get card info from scryfall
        do {
            echo 'Pre-populate fields with information from Scryfall? [Y/n]' . PHP_EOL;
            $in = trim(strtolower($this->getline()));
            if ($in == 'y' || $in == '') {
                $scryfall_condition = 1;
                $scryfall_card = $this->request_scryfall_card($card->name);
            } else if ($in == 'n') {
                $scryfall_condition = 2;
            }
        } while ($scryfall_condition === 0);

        echo 'What is the card\'s rarity?' . ($scryfall_card ? " [{$scryfall_card->rarity}]" : '') . PHP_EOL;
        $card->rarity = ($t = $this->getline()) ? $t : $scryfall_card->rarity;
        echo 'What is the card\'s mana cost?' . ($scryfall_card ? " [{$scryfall_card->manaCost}]" : '') . PHP_EOL;
        $card->manaCost = ($t = $this->getline()) ? $t : $scryfall_card->manaCost;
        echo 'What is the card\'s type?' . ($scryfall_card ? " [{$scryfall_card->type}]" : '') . PHP_EOL;
        $card->type = ($t = $this->getline()) ? $t : $scryfall_card->type;
        echo "What is the card's text?" . ($scryfall_card ? " [{$scryfall_card->text}]" : '') . PHP_EOL;
        $card->text = ($t = $this->getline()) ? $t : $scryfall_card->text;
        echo "What is the card's flavour text?" . ($scryfall_card ? " [{$scryfall_card->flavourText}]" : '') . PHP_EOL;
        $card->flavourText = ($t = $this->getline()) ? $t : $scryfall_card->flavourText;
        echo "What is the card's power?" . ($scryfall_card ? " [{$scryfall_card->power}]" : '') . PHP_EOL;
        $card->power = ($t = $this->getline()) ? $t : $scryfall_card->power;
        echo "What is the card's toughness?" . ($scryfall_card ? " [{$scryfall_card->toughness}]" : '') . PHP_EOL;
        $card->toughness = ($t = $this->getline()) ? $t : $scryfall_card->toughness;
        echo "What is the card's layout?" . ($scryfall_card ? " [{$scryfall_card->layout}]" : '') . PHP_EOL;
        $card->layout = ($t = $this->getline()) ? $t : $scryfall_card->layout;
        echo "What is the card's colour identity?" . ($scryfall_card ? " [" . implode($scryfall_card->colourIdentity) . "]" : '') . PHP_EOL;
        $card->colourIdentity = ($t = $this->getline()) ? str_split(strtoupper($t)) : $scryfall_card->colourIdentity;

        if (!$this->interruptus)
            return $card;

        $this->interruptus = false;
    }
}
Card Manager - Command Line + Web
===
## Dependencies
You'll need a couple of things to run this program.
- A computer running Windows 7 + (for now!)
- [Python 3.7.4+](https://www.python.org/downloads/) with the program in ENV (it's a checkbox in the installer)
- Scrython
    - After installing python, press `Win+R`
    - In the run window, type `cmd`
    - A command prompt window will open. Type `pip install scrython`

## Usage
To run the program, run the `Card Manager.bat` file as you would any other application. You will be presented with the following options:
1. Add a custom card to database
    - You will be asked a series of questions, and be provided the opportunity to use an official card from Scryfall as the default.
2. Add a card from scryfall
    - You will be asked for the name of the card. It is case-sensitive. Type it exactly as it appears on the card you wish to add.
3. Remove a card from database
    - You will be asked for the name of the card to remove. This purges all entries of the card in the database.
4. Edit a card in database
    - You will be asked for the name of the card to edit. This allows you to change properties you have entered before.
5. Save changes
    - This saves all the changes you've made to the database. Will appear with a * if the database has been modified.
6. View cards
    - This opens a web interface which shows all the cards currently in the database organized in an HTML webpage. It is necessary to close the program and restart it to halt this operation, but this will not jeopardize the data.
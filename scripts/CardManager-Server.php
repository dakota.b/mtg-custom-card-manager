<?php
require './CardManagerClasses.php';

$cc = new CardCollection();
?>
<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body {
            background-color: #cccccc;
        }
        table {
            border: 1px dotted black;
            width: 100%;
        }

        tr {
            border-bottom: 1px dotted black;
        }

        td {
            text-align: center;
            width: 400px;
        }

        tr:nth-of-type(2n) {
            background-color: rgba(0, 0, 0, .05);
        }

        .G, .G caption {
            background-color: #ccffcc;
        }

        .W, .W caption {
            background-color: #eeeeee;
        }

        .U, .U caption {
            background-color: #ccccff;
        }

        .R, .R caption {
            background-color: #ffcccc;
        }

        .B, .B caption {
            background-color: #333333;
            color: #ffffff;
        }

        .caption {
            border: 2px solid black;
            font-size: 1.2em;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <?php
        $allegianceNames = array('G' => 'Green', 'R' => 'Red', 'B' => 'Black', 'U' => 'Blue', 'W' => 'White', '' =>'Colorless');
        foreach($cc->db as $allegiance => $cards) {
            $alleg = array_key_exists($allegiance, $allegianceNames) ? $allegianceNames[$allegiance] : $allegiance;
            echo <<<HTML
            <table class="$allegiance">
                <caption>$alleg</caption>
                <tr>
                    <th>
                        Card Name
                    </th>
                    <th>
                        Quantity
                    </th>
                </tr>
HTML;
            foreach($cards as $card) {
                echo <<<HTML
                <tr><td>{$card->name}</td><td>{$card->quantity}</td></tr>
HTML;
            }

            echo '</table>';
        }
    ?>
</body>
</html>
